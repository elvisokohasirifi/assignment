public class Assignment{

  /**
   *@ author Elvis Okoh-Asirifi
   */
  
  /**
   * This method calculates the factorial of a given number
   * @param num This is the number whose factorial is to calculated
   * @return It returns the factorial of the number
   */
  public static int fact(int num){
    if (num <= 1) return 1;
    return num * fact(num - 1);
  }
  
  /**
   * This method draws the multiplication table for a given number
   * @param n This is the number whose multiplication table is to be drawn
   */
  public static void table(int n){
    for(int i = 1; i <=12; i++)
      System.out.println(n + " * " + i + " = " + (n*i));
  }
 
  /**
   * This is the main method. It must be called with an array as parameters
   * @param elvis This is the array of command line arguments
   */
  public static void main(String elvis[]){
    if(elvis.length == 0){
       System.out.println("To calculate the factorial of  a number, type \'fact number\'");
      System.out.println("To show multiplication tables for a number, type \'mult number\'");
      System.out.println("To find the maximum of a set of numbers, type \'max\' followed by the numbers separated by spaces");
      return;
    }
    
    String n = elvis[0];
    if (n.equalsIgnoreCase("table")) table(Integer.parseInt(elvis[1]));
    
    else if  (n.equalsIgnoreCase("fact")) System.out.println(fact(Integer.parseInt(elvis[1])));
    
    else if  (n.equalsIgnoreCase("max")) {
      int[] arguments = new int[elvis.length - 1];
      for (int i = 1; i < elvis.length; i++)
        arguments[i-1] = Integer.parseInt(elvis[i]);
      System.out.println(max(arguments));
    }
    
    else{
      System.out.println("To calculate the factorial of  a number, type \'fact number \'");
      System.out.println("To show multiplication tables for a number, type \'mult number \'");
      System.out.println("To find the maximum of a set of numbers, type \'max \' followed by the numbers separated by spaces");
    }
   }
  
  /**
   * This method finds the maximum of a given set of numbers
   * @param nums This is the array of numbers out of which the maximum will be found
   * @return It returns the maximum of the numbers
   */
  public static int max(int[] nums){
    int max = nums[0];
    for (int i : nums){
      if (i > max) max = i;
    }
    return max;
  }
}